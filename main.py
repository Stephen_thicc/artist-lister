class fileFunctions:
    def __init__(self):
        self.temp = []
        self.songsInList = []
        self.artistList = []

    def reader(self, fileName):
        with open(fileName, "r") as file:
            for line in file:
                self.temp.append((line.rstrip('\n')))
    
    def numberRemover(self):
        for i in self.temp:
            self.songsInList.append((i.split("-", 1)[1]).strip())
    
    def artistDetacher(self):
        for i in self.songsInList:
            try:
                temp = ((i.split("by")[1]).lower()).strip()
                if temp in self.artistList:
                    self.artistList.append(temp)
                
                else:
                    self.artistList.append(temp)
            
            except:
                self.artistList.append(temp)

    def newFileMaker(self):
        for i in self.artistList:
            temp = i + ".txt"
            with open(temp, "w+") as tempFile:
                tempFile.close()
    
    def songAdder(self):
        for i in range(0, len(self.songsInList)):
            tempFileName = self.artistList[i] + ".txt"
            with open(tempFileName, "a") as tempFile:
                tempFile.write(self.songsInList[i].split("by")[0] + "\n")

temp = fileFunctions()

temp.reader("list.txt")
temp.numberRemover()
temp.artistDetacher()
temp.newFileMaker()
temp.songAdder()